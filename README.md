# Yolov8数据处理器2
### 简介：
该项目是Yolov8数据处理器的基础上增加了很多数据增强的功能，可以将标注好的数据集进行重命名(如0001，0002)，分配数据集比例，并生成一个yaml文件，数据增强(数据集*40！！！)，最后得到一个可直接用于yolov8训练的数据集  
### 使用：
首先，下载项目：git clone https://gitee.com/luodewen/yolov8-data-processor.git

你可以通过直接点击文件夹中的exe文件来启动，文件夹内还提供了项目源码供读者学习参考，界面如下:  
![Alt text](md_img/image-3.png)
![Alt text](md_img/image-4.png)
一般来说，你的数据集类别和本作者设置的默认类别不会一致，因此你需要点击”INIT配置“按钮去生成一个config.json文件，config.json文件如下：

![Alt text](md_img/image-5.png)

接下来正式演示使用，将使用一个标注好的数据集作为演示，文件信息如下：  
![Alt text](md_img/image.png)
总共4张图片及其标注文件，点“改文件夹”，文件夹选定数据集所在的文件夹，选择完成后界面：
![Alt text](md_img/image-6.png)
接着点击”数据x5“表示色彩变换（变亮，变暗，椒盐噪声，高斯噪声），原数据集文件夹demo旁边会多出一个demo_x5的文件夹，内容如下：
![Alt text](md_img/image-7.png)
![Alt text](md_img/image-8.png)
当前界面：
![Alt text](md_img/image-9.png)
接下来点击“改文件夹”，选择demo_x5,再点击“数据x8”,会生成demo_x5_x8文件夹，如下：
![Alt text](md_img/image-10.png)
![Alt text](md_img/image-11.png)
![Alt text](md_img/image-12.png)
这样一来，数据x40的操作就完成了（作者曾经将一个500张图片的数据集扩增到2万张图片，并训练出了很好的效果,这里要注意的是，一定要先点击x8，再点击x5，当然不是说不这样就会出bug了，只是......，你可以尝试一下），接下来你可以将文件夹更改为demo_x5_x8,并进行划分数据集，但是作者这里推荐先重新命名一下(毕竟名字太乱了)，
点击“改文件夹”将文件夹改为demo_x5_x8后，再点击“重新命名”，（这个过程并不会再次生成文件夹），结果如下：
![Alt text](md_img/image-13.png)
接着点击“划分集合”，得到一个新文件夹demo_x5_x8_splited：
![Alt text](md_img/image-14.png)
![Alt text](md_img/image-15.png)

被划分数据集的数据就是可以直接被训练的数据集了，下面来演示后续步骤：
复制demo_x5_x8_splited文件夹中的d.yaml文件地址，接下来直接将路径放到寻来你命令中再回车即可
![Alt text](md_img/image-16.png)

