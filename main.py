from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
import sys
from function import *
import json
from random import shuffle
import cv2
import copy
import yaml
from shutil import copyfile
import os
from win import Ui_Win_main
from info import Ui_Win_info
# class MyThread(QThread):
#     trigger = pyqtSignal(str)
#     def __int__(self):
#         super().__init__()
#     def run(self):
#         for i in range(100):
#             time.sleep(0.1)
#             self.trigger.emit(str(i))

class MyInfoWin(QMainWindow, Ui_Win_info):
    def __init__(self):
        super(MyInfoWin, self).__init__()
        self.setupUi(self)
class Window(QMainWindow, Ui_Win_main):
    def __init__(self):
        super(Window, self).__init__()
        self.setupUi(self)
        self.infoWin=MyInfoWin()
        self.src_path=""
        self.length=5
        self.ratio=[7,2,1]
        self.classes={"0": "eyes","1": "mouth"}
        self.btn_1.clicked.connect(self.change_dir)
        self.btn_2.clicked.connect(self.rename)
        self.btn_3.clicked.connect(self.splite_dir)
        self.btn_4.clicked.connect(self.x5)
        self.btn_5.clicked.connect(self.x8)
        self.btn_6.clicked.connect(self.distort_dir)
        self.btn_7.clicked.connect(self.init_conf)
        self.btn_info.clicked.connect(self.showInfo)
        self.set_tb_text("info","---------------------------------------------------")
        self.set_tb_text("info","软件初始化成功......")
        self.set_tb_text("info","默认命名为5位数:00001,...,99999")
        self.set_tb_text("info","默认比例为=训练集:测试集:验证集:=7:2:1")
        self.set_tb_text("info","如需更改命名长度（仅支持3-7）或者比例（和为需要为10），请编辑config.json文件，仅支持数字3-7")
        self.read_conf()
        self.set_tb_text("info","---------------------------------------------------")

    #btn1 功能
    def change_dir(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            self.src_path = QtWidgets.QFileDialog.getExistingDirectory(None,"图片集合所在文件夹","./")
            self.set_tb_text("info","源文件夹改为:"+self.src_path)
            self.set_tb_text("info","---------------------------------------------------")
        except:
            self.set_tb_text("error","选择文件夹模块出错！！！")
            self.set_tb_text("info","---------------------------------------------------")

    #btn2 功能
    def rename(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            if self.src_path=="":
                self.set_tb_text("warn","请先先择图片所在文件夹")
            else:
                self.set_tb_text("info","开始重命名，当前文件夹:"+self.src_path)
                len_list=["%03d","%04d","%05d","%06d","%07d"]
                len_num=len_list[self.length-3]
                img_list,label_list=[],[]
                for file in os.listdir(self.src_path):
                    if file.split(".")[-1]=="jpg":img_list.append(file)
                    if file.split(".")[-1]=="txt":label_list.append(file)

                num = 1
                for file in img_list:
                    s =  len_num% num
                    os.rename(os.path.join(self.src_path, file), os.path.join(self.src_path, str(s) + "."+file.split(".")[1]))
                    self.set_tb_text("info",file+"->"+str(s) + "."+file.split(".")[1])
                    num += 1
                
                num = 1
                for file in label_list:
                    s = len_num % num
                    os.rename(os.path.join(self.src_path, file), os.path.join(self.src_path, str(s) + "."+file.split(".")[1]))
                    self.set_tb_text("info",file+"->"+str(s) + "."+file.split(".")[1])
                    num += 1
            self.set_tb_text("info","---------------------------------------------------")

        except:
            self.set_tb_text("error","重命名模块出错！！！")
            self.set_tb_text("info","---------------------------------------------------")

    #btn3 功能
    def splite_dir(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            if self.src_path=="":self.set_tb_text("warn","请先先择图片所在文件夹")
            else:
                self.set_tb_text("info","开始划分数据集，当前文件夹:"+self.src_path)
                names = os.listdir(self.src_path)
                train=names[0:int(self.ratio[0]*len(names)/10)]
                test=names[int(self.ratio[0]*len(names)/10):int((self.ratio[0]+self.ratio[1])*len(names)/10)]
                val=names[int((self.ratio[0]+self.ratio[1])*len(names)/10):int(len(names))]
                # print(len(train),len(test),len(val))
                for p1,p2 in zip(["test/","val/","train/"],[test,val,train]):
                    if not os.path.exists(self.src_path+"_splited/"+p1+"images"):os.makedirs(self.src_path+"_splited/"+p1+"images")
                    if not os.path.exists(self.src_path+"_splited/"+p1+"labels"):os.makedirs(self.src_path+"_splited/"+p1+"labels")
                    for i in p2:
                        dest_floder="labels/" if i.split(".")[-1]=='txt' else "images/"
                        copyfile(self.src_path+"/"+i,self.src_path+"_splited"+"/"+p1+dest_floder+i)
                        self.set_tb_text("info",p1+" "+i)
                self.set_tb_text("info","图片总数量="+str(int(len(names)/2))+"/train:test:val="+str(int(len(train)/2))+":"+str(int(len(test)/2))+":"+str(int(len(val)/2)))            
                apiData = {"train": self.src_path+"_splited\\train","test": self.src_path+"_splited\\test",
                        "val": self.src_path+"_splited\\val","names": self.classes
                }
                with open(self.src_path+"_splited/d.yaml", 'w', encoding='utf-8') as f:
                    yaml.dump(data=apiData, stream=f, allow_unicode=True)
            self.set_tb_text("info","---------------------------------------------------")
        except:
            self.set_tb_text("error","划分数据模块出错！！！")
            self.set_tb_text("info","---------------------------------------------------")
    #btn5 功能
    def x8(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            if self.src_path=="":
                self.set_tb_text("warn","请先先择图片所在文件夹")
                self.set_tb_text("info","---------------------------------------------------")
            else:
                self.set_tb_text("info","开始色彩变换，当前文件夹:"+self.src_path)
                dest_floder=self.src_path+"_x8"
                names=os.listdir(self.src_path)
                if not os.path.exists(dest_floder): os.mkdir(dest_floder) 
                for name in names:
                    if name.split(".")[1]=="jpg" or name.split(".")[1]=="png":
                        self.set_tb_text("info",name)
                        img=cv2.imread(self.src_path+"/"+name)
                        img2,img3,img4=cv2.flip(img,1),cv2.flip(img,0),cv2.flip(img,-1)
                        img_1 = cv2.transpose(img)    # 转置（行列互换）
                        img_2,img_3,img_4=cv2.flip(img_1,1),cv2.flip(img_1,0),cv2.flip(img_1,-1)
                        all_img=[img,img2,img3,img4,img_1,img_2,img_3,img_4]
                        back_names=[".jpg","_f1.jpg","_f0.jpg","_f-1.jpg","_t.jpg","_t_f1.jpg","_t_f0.jpg","_t_f-1.jpg"] \
                            if name.split(".")[1]=="jpg"\
                            else   [".png","_f1.png","_f0.png","_f-1.png","_t.png","_t_f1.png","_t_f0.png","_t_f-1.png"]        
                        for i,n in zip(all_img,back_names):
                            cv2.imwrite(dest_floder+"/"+name.split(".")[0]+n,i)
                            
                    if name.split(".")[1]=="txt":
                        with open(self.src_path+"/"+name,"r") as f:
                            txt=f.readlines()
                        info=[]
                        for index,i in enumerate(txt):
                            info.append({"class":i.split(" ")[0],"p":[float(i.split(" ")[1]),float(i.split(" ")[2]),float(i.split(" ")[3]),float(i.split(" ")[4][0:-1])]})

                        info2=copy.deepcopy(info)
                        for i in info2:
                            i["p"][0]=1-i["p"][0]
                        info3=copy.deepcopy(info)
                        for i in info3:
                            i["p"][1]=1-i["p"][1]
                        info4=copy.deepcopy(info)
                        for i in info4:
                            i["p"][1]=1-i["p"][1]    
                            i["p"][0]=1-i["p"][0]   
                        # 旋转90度
                        info_1=copy.deepcopy(info)
                        for i in info_1:
                            temp=i["p"][0]   
                            i["p"][0] =i["p"][1]   
                            i["p"][1] =temp
                            temp=i["p"][2]   
                            i["p"][2] =i["p"][3]   
                            i["p"][3] =temp  
                        info_2=copy.deepcopy(info_1)
                        for i in info_2:
                            i["p"][0]=1-i["p"][0]
                        info_3=copy.deepcopy(info_1)
                        for i in info_3:
                            i["p"][1]=1-i["p"][1]
                        info_4=copy.deepcopy(info_1)
                        for i in info_4:
                            i["p"][1]=1-i["p"][1]    
                            i["p"][0]=1-i["p"][0]   
                        all_info=[info,info2,info3,info4,info_1,info_2,info_3,info_4]
                        back_names=[".txt","_f1.txt","_f0.txt","_f-1.txt","_t.txt","_t_f1.txt","_t_f0.txt","_t_f-1.txt"]
                        for i,n in zip(all_info,back_names):
                            str1=""
                            for j in i:
                                str1+=j["class"]+" "'%.6f'%j["p"][0]+" "+'%.6f'%j["p"][1]+" "+'%.6f'%j["p"][2]+" "+'%.6f'%j["p"][3]+"\n"
                            with open(dest_floder+"/"+name.split(".")[0]+n,"w") as f:
                                f.write(str1)
            self.set_tb_text("info","---------------------------------------------------")
        except:
            self.set_tb_text("error","x8模块出错！！！")
            self.set_tb_text("info","---------------------------------------------------")
    #btn4 功能
    def x5(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            if self.src_path=="":
                self.set_tb_text("warn","请先先择图片所在文件夹")
            else:
                self.set_tb_text("info","开始图形变换，当前文件夹:"+self.src_path)
                dest_floder=self.src_path+"_x5"
                if not os.path.exists(dest_floder): os.mkdir(dest_floder) 
                names=os.listdir(self.src_path)
                for name in names:
                    shutil.copy(self.src_path+"/"+name, dest_floder+"/"+name)
                    if name.split(".")[1]=="jpg" or name.split(".")[1]=="png":
                        self.set_tb_text("info",name)
                        img=cv2.imread(self.src_path+"/"+name)
                        back_names=["_sp.jpg","_gaussian.jpg","_turn_light.jpg","_turn_dark.jpg"] if name.split(".")[1]=="jpg"\
                            else ["_sp.png","_gaussian.png","_turn_light.png","_turn_dark.png"]
                        all_img=[sp_noise(deepcopy(img)),gaussian_noise(deepcopy(img)),turn_light(deepcopy(img)),turn_dark(deepcopy(img))]
                        for i,n in zip(all_img,back_names):
                            cv2.imwrite(dest_floder+"/"+name.split(".")[0]+n,i)
                            
                    if name.split(".")[1]=="txt":
                        back_names=["_sp.txt","_gaussian.txt","_turn_light.txt","_turn_dark.txt"]
                        for i in back_names:
                            shutil.copy(self.src_path+"/"+name, dest_floder+"/"+name.split(".")[0]+i)
            self.set_tb_text("info","---------------------------------------------------")
        except:
            self.set_tb_text("error","x5模块出错！！！")
            self.set_tb_text("info","---------------------------------------------------")

    #btn6 功能
    def distort_dir(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            if self.src_path=="":
                self.set_tb_text("warn","请先先择图片所在文件夹")
            else:
                self.set_tb_text("info","开始打乱，当前文件夹:"+self.src_path)
                names=os.listdir(self.src_path)
                l1=[i for i in range(int(len(names)/2))]
                shuffle(l1)
                dest_floder=self.src_path+"_distorted"
                if not os.path.exists(dest_floder): os.mkdir(dest_floder) 

                just_names=[]
                for name in names:
                    just_names.append(name.split(".")[0])
                just_names=list(set(just_names))#将列表转换为集合再转换为列表（集合没有重复项）
                for name,l in zip(just_names,l1):
                    self.set_tb_text("info",name)
                    copyfile(self.src_path+"/"+name+".txt",dest_floder+"/"+str(l)+".txt")
                    if os.path.exists(self.src_path+"/"+name+".jpg"):
                        copyfile(self.src_path+"/"+name+".jpg",dest_floder+"/"+str(l)+".jpg")
                    else:copyfile(self.src_path+"/"+name+".png",dest_floder+"/"+str(l)+".png")
            self.set_tb_text("info","---------------------------------------------------")
        except:
            self.set_tb_text("error","乱序文件模块出错！！！")
            self.set_tb_text("info","---------------------------------------------------")

    #btn7 功能
    def init_conf(self):
        try:
            self.set_tb_text("info","---------------------------------------------------")
            conf_dict = {"name_length": 5,"ratio":[7,2,1],"names": {0: "eyes",1: "mouth"}}
            with open("config.json","w") as f:
                json.dump(conf_dict,f)
            self.set_tb_text("info","配置文件config.json生成成功，请重新打开本程序")
            self.set_tb_text("info","---------------------------------------------------")
        except:
            self.set_tb_text("error","初始化配置模块出错!!!")
            self.set_tb_text("info","---------------------------------------------------")
    #btn_info 功能
    def showInfo(self):
        try:
            self.infoWin.show()
        except:self.set_tb_text("error","显示软件信息模块出错！！！")

    def read_conf(self):
        try:
            if os.path.exists("config.json"):
                with open("config.json",'r') as load_f:
                    conf_dict = json.load(load_f)
            if conf_dict["name_length"] in [3,4,5,6,7]:
                if conf_dict["ratio"][0]+conf_dict["ratio"][1]+conf_dict["ratio"][2]==10:
                    self.length,self.ratio,self.classes=conf_dict["name_length"],conf_dict["ratio"],conf_dict["names"]
                    self.set_tb_text("info","配置读取成功")
                else:
                    self.set_tb_text("warn","请确保ratio的三个数字和=10")
            else:
                self.set_tb_text("warn","请确保name_length取值为3，4，5，6，7,本次运行将采用系统默认配置")
        except:self.set_tb_text("warn","未知检测到config.json，您可按下init配置重新生成，或者使用默认配置继续")
    def set_tb_text(self,cate,txt):
        QApplication.processEvents()
        # self.tb.setText ("<font color=\"#008888\">" +txt+ "</font>")
        if cate=="info":start="<font color=\"#00FF00\">[info]</font> "
        if cate=="warn":start="<font color=\"#FFFF00\">[warn]</font> "
        if cate=="error":start="<font color=\"#FF0000\">[error]</font> "
        self.tb.append (start+"<font color=\"#009999\">" +txt+ "</font>")

if __name__ == '__main__':
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    app = QApplication(sys.argv)
    father = Window()
    father.show()
    sys.exit(app.exec_())
