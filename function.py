import os
import cv2
import numpy as np
import random
from copy import deepcopy
import shutil
# 先进行x8操作，再进行x5操作，因为前者对文件的内部更改更多
def sp_noise(img):
    proportion=0.3
    height, width = img.shape[0], img.shape[1]#获取高度宽度像素值
    num = int(height * width * proportion) #一个准备加入多少噪声小点
    for i in range(num):
        w = random.randint(0, width - 1)
        h = random.randint(0, height - 1)
        if random.randint(0, 1) == 0:
            img[h, w] = 0
        else:
            img[h, w] = 255
    return img
def gaussian_noise(img):
    mean, sigma=0,0.3
    img = img / 255
    noise = np.random.normal(mean, sigma, img.shape)
    gaussian_out = img + noise
    gaussian_out = np.clip(gaussian_out, 0, 1)
    gaussian_out = np.uint8(gaussian_out*255)
    return gaussian_out
def turn_light(img):
    add=70
    for i in img:
        for j in i:
            j[0]=255 if j[0]+add>255 else j[0]+add
            j[1]=255 if j[1]+add>255 else j[1]+add
            j[2]=255 if j[2]+add>255 else j[2]+add
    return img    
def turn_dark(img):
    add=-70
    for i in img:
        for j in i:   
            j[0]=0 if j[0]+add<0 else j[0]+add
            j[1]=0 if j[1]+add<0 else j[1]+add
            j[2]=0 if j[2]+add<0 else j[2]+add
    return img    